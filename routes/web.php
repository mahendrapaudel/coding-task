<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




//
Route::get('/', function () {
    return redirect('dashboard');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/api/v1/departments', 'Api\V1\DepartmentController@index')->name('department.index')->middleware('auth');
Route::post('/api/v1/departments/auto-complete','Api\V1\DepartmentController@autocomplete')->name('departments.autocomplete')->middleware('auth');
Route::post('/api/v1/employees/department', 'Api\V1\EmployeeController@get_employees_by_department')->name('employees.department');
Route::resource('/api/v1/department', 'Api\V1\DepartmentController',['except' => ['index','create', 'edit']]);

Route::post('/api/v1/users', 'Api\V1\UserController@index')->name('user.index');
Route::post('/api/v1/users/department', 'Api\V1\UserController@get_users_by_department')->name('users.department');
Route::resource('/api/v1/user', 'Api\V1\UserController',['except' => ['index','create', 'edit']]);

Route::post('/api/v1/employees', 'Api\V1\EmployeeController@index')->name('employee.index');
Route::resource('/api/v1/employee', 'Api\V1\EmployeeController',['except' => ['index','create', 'edit']]);
Route::post('/api/v1/employee/export_to_csv', 'Api\V1\EmployeeController@export_to_csv')->name('employee.export');


// Route::group(['prefix' => '/api/v1', ['middleware' => ['auth']],'namespace' => 'Api\V1', 'as' => 'api.'], function () {
//   Route::resource('user', 'UserController', ['except' => ['create', 'edit']]);
//    Route::resource('department', 'DepartmentController');
// });



Route::get('/dashboard', 'HomeController@index')->name('dashboard');

/*
* routes that redirect to vuejs default component
*
*/

Route::get('{slug}/list',function(){
    return view('home',['userInfo' => ['userID'=>\Auth::User()->user_id,'userName'=>\Auth::User()->name,'email'=>\Auth::User()->email]]);
})->middleware('auth');

Route::get('{slug}/{id}/details',function(){
    return view('home',['userInfo' => ['userID'=>\Auth::User()->user_id,'userName'=>\Auth::User()->name,'email'=>\Auth::User()->email]]);
})->middleware('auth');
