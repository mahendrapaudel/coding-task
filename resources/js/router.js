import Vue from "vue";
import Router from "vue-router";
import MainView from "./views/Main.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path : '/:module',
      name : "main",
      component : MainView
    },
    {
      path : '/:module/:slug',
      name : "add_departemnt",
      component : MainView
    },
    {
      path : '/:module/:id/:slug',
      name : "add_department",
      component : MainView
    },
    {
      path: "/register",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.

    },
  ]
});
