<?php

namespace App\Http\Controllers\Api\V1;
use App\Department;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Collection;

/**
 * @group Departments Management
 *
 * APIs for managing departments
 */
class DepartmentController extends Controller
{
  /**
         * @bodyParam {"page":1,"limit":5,"limitOptions":[5,10,15,20],"search":{"field":"name","value":""}} object required name.
         * @response {
         *  "status": "success",
         *  "result": {
         *   "total": 12,
         *   "rows" : [{
         *        "department_id": 2,
         *        "name": null,
         *        "description": "test description",
         *        "bonus_rate": 5,
         *        "deleted_at": null,
         *        "created_at": "2018-12-06 15:11:42",
         *        "updated_at": "2018-12-06 15:11:42"
         *    },
         *    {
         *        "department_id": 2,
         *        "name": null,
         *        "description": "test description",
         *        "bonus_rate": 5,
         *        "deleted_at": null,
         *        "created_at": "2018-12-06 15:11:42",
         *        "updated_at": "2018-12-06 15:11:42"
         *    }]
         *  },
         *  "messages": null
         * }
        */

       public function index(Request $request, Department $department)
       {
         $department = $department->getDepartments($request);
         return response()->json([
            'status' => 'success',
            'result' => [
                'total' => $department->total(),
                'rows' => $department->items()
            ],
            'messages' => null
          ]);
       }


     /**
       * @bodyParam name string required name.
       * @bodyParam description string required description.
       * @bodyParam bonus_rate decimal required bonus_rate.
       * @response {
       *  "status": "success",
       *  "result": {
       *  "department_id": 2,
       *  "name": null,
       *  "description": "test description",
       *  "bonus_rate": 5,
       *  "deleted_at": null,
       *  "created_at": "2018-12-06 15:11:42",
       *  "updated_at": "2018-12-06 15:11:42"
       *   },
       *  "messages": null
       * }
      */
      public function store(Request $request)
      {
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'bonus_rate' => 'required|numeric'
        ];

        $validator = Validator::make($request->all(), $rules);
          if (!$validator->fails()) {
            $department = new Department;
            $department->name =  $request->name;
            $department->description =  $request->description;
            $department->bonus_rate =  $request->bonus_rate;
            $department->save();
            return response()->json([
                  'status' => 'success',
                  'result' => $department,
                  'messages' => null
                  ], 201);
              } else {
                return response()->json([
                  'status' => 'error',
                  'result' => $validator->messages(),
                  'messages' => null
                ]);
              }
      }

      /**
      * @bodyParam department_id int required the ID of the Department
      * @response {
      *  "status": "success",
      *  "result": {
      *  "department_id": 2,
      *  "name": null,
      *  "description": "test description",
      *  "bonus_rate": 5,
      *  "deleted_at": null,
      *  "created_at": "2018-12-06 15:11:42",
      *  "updated_at": "2018-12-06 15:11:42"
      *   },
      *  "messages": null
      * }
     */
    public function show(Department $department)
    {
      return response()->json([
            'status' => 'success',
            'result' => $department,
            'messages' => null
          ], 200);
    }


    /**
      * @bodyParam name string required The title of the post.
      * @bodyParam email email required The title of the post.
      * @bodyParam date_of_birth date optional  The type of post to create. Defaults to 'textophonious'.
      * @bodyParam department_id int required the ID of the Department
      * @bodyParam tracker_type_id int required  the ID of the Tracker Type
      * @bodyParam dashboard_type int optional
      * @bodyParam password string required Password
      * @response {
      *  "status": "success",
      *  "result": {
      *  "department_id": 2,
      *  "name": null,
      *  "description": "test description",
      *  "bonus_rate": 5,
      *  "deleted_at": null,
      *  "created_at": "2018-12-06 15:11:42",
      *  "updated_at": "2018-12-06 15:11:42"
      *   },
      *  "messages": null
      * }
     */
    public function update( Department $department, Request $request )
    {
      $rules = [
        'name' => 'required',
        'description' => 'required',
        'bonus_rate' => 'required|numeric'
      ];


      $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {
          $department->name =  $request->name;
          $department->description =  $request->description;
          $department->bonus_rate =  $request->bonus_rate;
          $department->update();
          return response()->json([
                'status' => 'success',
                'result' => $department,
                'messages' => null
              ], 200);
        } else {
          return response()->json([
              'status' => 'error',
              'result' => $validator->messages(),
              'messages' => null
          ]);
      }
    }

    /**
      * @bodyParam department_id int required the ID of the Department
      * @response {
      *  "status": "success",
      *  "result": "null",
      *  "messages": null
      * }
     */

    public function destroy(Department $department)
    {
      $department->delete();
      return response()->json([
            'status' => 'success',
            'result' => 'null',
            'messages' => null
          ], 200);
    }

    /**
      * @bodyParam department_id int required the ID of the Department
      * @response {
      * {"status":"success",
      * "result":[
      * {"value":14,"caption":"new customer title"},
      * {"value":13,"caption":"dsafdaf"},
      * {"value":12,"caption":"jjkj edited"},
      * {"value":2,"caption":"The yy Group"}
      * ],
      * "messages":null
      * }
      * }
     */

    public function autocomplete(Department $department)
    {
      $departments=$department->getDepartmentsAll();
      return response()->json([
            'status' => 'success',
            'result' => $departments,
            'messages' => null
          ], 200);

    }
}
