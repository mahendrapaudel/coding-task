<?php

namespace App\Http\Controllers\Api\V1;
use App\Employee;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
use Response;
use StreamedResponse;
/**
 * @group employees Management
 *
 * APIs for managing employees
 */
class EmployeeController extends Controller
{
  /**
         * @bodyParam {"page":1,"limit":5,"limitOptions":[5,10,15,20],"search":{"field":"name","value":""}} object   required name.
          *  @response {
          * "status": "success",
          * "result": {
          * "total": 8,
          * "rows": [
          *     {
          *       "employee_id": 1,
          *       "department_id": 1,
          *       "name": "last test",
          *       "photo": null,
          *       "gender": "male",
          *       "email": "",
          *       "phone": "34343",
          *       "address":"test address",
          *       "educational_background":"test educational_background",
          *       "current_salary":"100000"
          *       "deleted_at": null,
          *       "created_at": "2018-12-13 09:39:45",
          *       "updated_at": "2018-12-13 09:39:45"
          *     },
          *     {
          *       "employee_id": 2,
          *       "department_id": 2,
          *       "name": "last test",
          *       "photo": null,
          *       "gender": "male",
          *       "email": "",
          *       "phone": "34343",
          *       "address":"test address",
          *       "educational_background":"test educational_background",
          *       "current_salary":"100000"
          *       "deleted_at": null,
          *       "created_at": "2018-12-13 09:39:45",
          *       "updated_at": "2018-12-13 09:39:45"
          *      }
          *   ]
          *  },
          *  "messages": null
          *  }
        */

       public function index(Request $request, Employee $employee)
       {
         $employee = $employee->getEmployees($request);
          return response()->json([
            'status' => 'success',
            'result' => [
                'total' => $employee->total(),
                'rows' => $employee->items()
            ],
            'messages' => null
          ]);
       }


     /**
       * @bodyParam name string required name.
       * @bodyParam department_id int required department_id, is a foreign key.
       * @bodyParam api_key int required api_key.
       * @bodyParam description string optional Description of Employee.
       * @bodyParam photo file optional Logo of Employee.
       * @response {
       *   "status": "success",
       *   "result": {
       *       "employee_id": 1,
       *       "department_id": 1,
       *       "name": "last test",
       *       "photo": null,
       *       "gender": "male",
       *       "email": "",
       *       "phone": "34343",
       *       "address":"test address",
       *       "educational_background":"test educational_background",
       *       "current_salary":"100000"
       *       "deleted_at": null,
       *       "created_at": "2018-12-13 09:39:45",
       *       "updated_at": "2018-12-13 09:39:45"
       *             },
       *       "messages": null
       *     }
      */
      public function store(Request $request)
      {
        $rules = [
            'name' => 'required',
            'department_id' => 'required',
            'gender' => 'required',
            'photo1' => 'mimes:jpeg,bmp,png,ico',
            'email' => 'required',
            'phone' => 'required',
            'address'=>'required',
            'educational_background'=>'required',
            'current_salary'=>'required'
        ];



        $validator = Validator::make($request->all(), $rules);
          if (!$validator->fails()) {
            if($request->file('photo1')) {

             $file = $request->file('photo1');
             // generate a new filename. getClientOriginalExtension() for the file extension
              $filename = 'photo-image-' . time() . '.' . $file->getClientOriginalExtension();

              // save to storage/app/photos as the new $filename
              //  $path_file = $file->storeAs('photos', $filename);'file.txt', 'Contents'
              $contents=file_get_contents($request->file('photo1')->getRealPath());
              $path=Storage::disk('public')->put($filename, $contents);
              if(!$path) {
              return false;
              }


              $path=$filename;
            }else {
              $path="";
            }
            $employee = new Employee;
            $employee->name =  $request->name;
            $employee->department_id =  $request->department_id;
            $employee->gender =  $request->gender;
            $employee->photo =  $path;
            $employee->email =  $request->email;
            $employee->phone =  $request->phone;
            $employee->address =  $request->address;
            $employee->educational_background =  $request->educational_background;
            $employee->address =  $request->address;
            $employee->current_salary =  $request->current_salary;
            $employee->save();
            return response()->json([
                  'status' => 'success',
                  'result' => $employee,
                  'messages' => null
                  ], 201);
              } else {
                return response()->json([
                  'status' => 'error',
                  'result' => $validator->messages(),
                  'messages' => null
                ]);
              }
      }

      /**
      * @bodyParam employee_id int required the ID of the employee
      * @response {
      *  "status": "success",
      *  "result": {
      *       "employee_id": 1,
      *       "department_id": 1,
      *       "name": "last test",
      *       "photo": null,
      *       "gender": "male",
      *       "email": "",
      *       "phone": "34343",
      *       "address":"test address",
      *       "educational_background":"test educational_background",
      *       "current_salary":"100000"
      *       "deleted_at": null,
      *       "created_at": "2018-12-13 09:39:45",
      *       "updated_at": "2018-12-13 09:39:45"
      *          },
      *     "messages": null
      *    }
     */
    public function show(Employee $employee)
    {
      return response()->json([
            'status' => 'success',
            'result' => $employee,
            'messages' => null
          ], 200);
    }


    /**
      * @bodyParam employee_id integer required Id of employee which needs to updated.
      * @bodyParam name string required The title of the employee.
      * @bodyParam api_key string required The title of the employee.
      * @bodyParam department_id integer required, is foreign key.
      * @bodyParam description string required for the description of the employee
      * @bodyParam photo file optional for the photo of the employee
      * @response {
      *      "status": "success",
      *      "result": {
      *       "employee_id": 1,
      *       "department_id": 1,
      *       "name": "last test",
      *       "photo": null,
      *       "gender": "male",
      *       "email": "",
      *       "phone": "34343",
      *       "address":"test address",
      *       "educational_background":"test educational_background",
      *       "current_salary":"100000"
      *       "deleted_at": null,
      *       "created_at": "2018-12-13 09:39:45",
      *       "updated_at": "2018-12-13 09:39:45"
      *          },
      *      "messages": null
      *   }
     */
    public function update( Employee $employee, Request $request )
    {
      $rules1 = [
      'name' => 'required',
      'department_id' => 'required',
      'gender' => 'required',
      'email' => 'required',
      'phone' => 'required',
      'address'=>'required',
      'educational_background'=>'required',
      'current_salary'=>'required'
    ];

      $rules2=[];
      if($request->file('photo1')) {
       $rules2 = ['photo1' => 'mimes:jpeg,bmp,png,ico'];
     }

      $rules=array_merge($rules1,$rules2);


      $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {
          if($request->file('photo1')) {
            $file = $request->file('photo1');
            // generate a new filename. getClientOriginalExtension() for the file extension
             $filename = 'photo-image-' . time() . '.' . $file->getClientOriginalExtension();

             $contents=file_get_contents($request->file('photo1')->getRealPath());
             $path=Storage::disk('public')->put($filename, $contents);
             if(!$path) {
             return false;
             }
             $path=$filename;
             $old_image=$employee->photo;
           }else {
             $path="";
             $old_image="";
           }

           $employee->name =  $request->name;
           $employee->department_id =  $request->department_id;
           $employee->gender =  $request->gender;
           $employee->email =  $request->email;
           $employee->phone =  $request->phone;
           $employee->address =  $request->address;
           $employee->educational_background =  $request->educational_background;
           $employee->address =  $request->address;
           $employee->current_salary =  $request->current_salary;

          if($path){
          $employee->photo =  $path;
        }

          $employee->update();

        if($old_image) {
          Storage::disk('public_uploads')->delete($old_image);
        }

          return response()->json([
                'status' => 'success',
                'result' => $employee,
                'messages' => null
              ], 200);
        } else {
          return response()->json([
              'status' => 'error',
              'result' => $validator->messages(),
              'messages' => null
          ]);
      }
    }

    /**
      * @bodyParam employee_id int required the ID of the employee
      * @response {
      *  "status": "success",
      *  "result": "null",
      *  "messages": null
      * }
     */

    public function destroy(Employee $employee)
    {
      $photo=$employee->photo;
      $employee->delete();
      Storage::disk('public_uploads')->delete($photo);
      return response()->json([
            'status' => 'success',
            'result' => 'null',
            'messages' => null
          ], 200);
    }

    /**
           * @bodyParam {"page":1,"limit":5,"limitOptions":[5,10,15,20],"search":{"field":"name","value":""}} object   required name.
            *  @response {
            * "status": "success",
            * "result": {
            * "total": 8,
            * "rows": [
            *     {
            *       "employee_id": 1,
            *       "department_id": 1,
            *       "name": "last test",
            *       "photo": null,
            *       "gender": "male",
            *       "email": "",
            *       "phone": "34343",
            *       "address":"test address",
            *       "educational_background":"test educational_background",
            *       "current_salary":"100000"
            *       "deleted_at": null,
            *       "created_at": "2018-12-13 09:39:45",
            *       "updated_at": "2018-12-13 09:39:45"
            *      },
            *     {
            *       "employee_id": 1,
            *       "department_id": 1,
            *       "name": "last test",
            *       "photo": null,
            *       "gender": "male",
            *       "email": "",
            *       "phone": "34343",
            *       "address":"test address",
            *       "educational_background":"test educational_background",
            *       "current_salary":"100000"
            *       "deleted_at": null,
            *       "created_at": "2018-12-13 09:39:45",
            *       "updated_at": "2018-12-13 09:39:45"
            *     }
            *   ]
            *  },
            *  "messages": null
            *  }
          */

         public function get_employees_by_department(Request $request, Employee $employee)
         {
          $department_id=$request->department_id;
           $employee = $employee->getEmployeesClients($request,$department_id);
            return response()->json([
              'status' => 'success',
              'result' => [
                  'total' => $employee->total(),
                  'rows' => $employee->items()
              ],
              'messages' => null
            ]);
         }

         /**
           * @response {
           *  "status": "success",
           *  "result": "null",
           *  "messages": null
           * }
          */
         public function export_to_csv(Request $request, Employee $employee)
         {
           $headers = array(
                'Content-Type'        => 'text/csv',
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
                'Content-Disposition' => 'attachment; filename=employees.csv',
                'Expires'             => '0',
                'Pragma'              => 'public',
            );

             $employees = $employee->getAllEmployees($request);
             $columns=array('Name', 'Photo', 'Gender', 'Email','Phone','Address','Educational Background','Department','Current Salary','Bonus');

             // Open output   stream
          $handle = fopen('employees.csv', 'w+');

          // Add CSV headers
          fputcsv($handle, $columns);


            foreach ($employees as $row) {
                // Add a new row with data
                fputcsv($handle, array($row['name'], $row['photo'],$row['gender'], $row['email'], $row['phone'],$row['address'],$row['educational_background'],$row['departmentName'],$row['current_salary'],$row['bonus']));
            }


         // Close the output stream
         fclose($handle);

         return response()->json([
               'status' => 'success',
               'result' => null,
               'messages' => null
             ], 200);
    }
}
