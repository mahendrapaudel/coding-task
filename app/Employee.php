<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Employee extends Model
{

    protected $table = 'employees';
    protected $primaryKey = 'employee_id';
    protected $fillable = ['department_id','gender','name','email','phone','photo','address','educational_background','current_salary'];



    public function replaceField($field, $fields = [])
    {
        if (in_array($field, $fields)) {
            return $fields[$field];
        }

        return $field;
    }

    /**
     * Get List of Employees
     * @access  public
     * @param
     * @return  json(array)
     */

    public function getEmployees($request)
    {
        $employees = $this->select(['employees.*','departments.name as departmentName',DB::raw('Round((employees.current_salary*departments.bonus_rate/100),2) as bonus')]);
        if (!empty($request->search['field'])) {
            $searchField = $request->search['field'];
            $searchValue = $request->search['value'];
            $employees->where($searchField, 'like', '%' . $searchValue . '%');
        }
        $employees->join('departments', 'departments.department_id', '=', 'employees.department_id');
        $employees->orderBy('employees.employee_id', 'desc');

        return $employees->paginate($request->limit);
    }

    public function getEmployeesClients($request,$department_id)
    {
        $employees = $this->select(['*']);
        if (!empty($request->search['field'])) {
            $searchField = $request->search['field'];
            $searchValue = $request->search['value'];
            $employees->where($searchField, 'like', '%' . $searchValue . '%');
        }
        $employees->orderBy('employees.department_id', 'desc');
        $employees->where('department_id',$department_id);
        return $employees->paginate($request->limit);
    }

    /**
     * Get List of all Employees
     * @access  public
     * @param
     * @return  json(array)
     */

    public function getAllEmployees($request)
    {
        $employees = $this->select(['employees.*','departments.name as departmentName',DB::raw('Round((employees.current_salary*departments.bonus_rate/100),2) as bonus')]);
        if (!empty($request->search['field'])) {
            $searchField = $request->search['field'];
            $searchValue = $request->search['value'];
            $employees->where($searchField, 'like', '%' . $searchValue . '%');
        }
        $employees->join('departments', 'departments.department_id', '=', 'employees.department_id');
        $employees->orderBy('employees.employee_id', 'desc');

        return $employees->get();
    }

}
