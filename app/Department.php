<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{

    protected $table = 'departments';


     protected $fillable = [
         'name', 'description', 'bonus_rate'
     ];



     /**
    * Change Primary Key
    *
    *
    */
    protected $primaryKey = 'department_id';

    public function replaceField($field, $fields = [])
    {
        if (in_array($field, $fields)) {
            return $fields[$field];
        }

        return $field;
    }

    /**
     * Get List of Departments
     * @access  public
     * @param
     * @return  json(array)
     */

    public function getDepartments($request)
    {
        $departments = $this->select(['*']);
        if (!empty($request->search['field'])) {
            $searchField = $request->search['field'];
            $searchValue = $request->search['value'];
            $departments->where($searchField, 'like', '%' . $searchValue . '%');
        }
        $departments->orderBy('departments.department_id', 'desc');
        return $departments->paginate($request->limit);
    }

    /**
     * Get List of All Departments
     * @access  public
     * @param
     * @return  json(array)
     */
    public function getDepartmentsAll()
    {
        $departments = $this->select('department_id as value','name as caption');
        $departments->orderBy('departments.department_id', 'desc');
        return $departments->get();
    }

}
