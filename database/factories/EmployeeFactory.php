<?php

use Faker\Generator as Faker;

$factory->define(App\Employee::class, function (Faker $faker) {
    return [
        'name'    => $faker->name,
        'photo'   => $faker->image('./storage/app/public/', 100, 100, null, false),
        'gender'  => 'male',
        'email'   =>$faker->unique()->safeEmail,
        'phone'   =>$faker->phoneNumber,
        'address' =>$faker->address,
        'educational_background'=>$faker->text,
        'department_id'=>1,
        'current_salary'=>$faker->numberBetween($min =10000,$max=100000)
    ];
});
